<? 
$h1 = "Mapa do Site";
include('inc/head.php'); ?>
<? include('inc/header.php'); ?>

<style>
    .list-group-item:not(.active):hover {
        background: #2f55d4;
    }

    .list-group-item:hover a {
        color: #fff;
    }

@media (max-width: 576px){  
    .list-map{width: 100% !important};
}
</style>

<div class="container my-5 py-5">
    <h2 class="text-uppercase my-2 p-3 btn-primary text-white">Produtos</h2>
    <ul class="list-group p-5 w-50 list-map">
        <?
            foreach($categorias->getCategorias() as $categoria)
            {
                $categoriaSemAcento = $trata->trataAcentos($categoria);
                echo "<li class=\"list-group-item active text-uppercase\">".$trata->retiraHifen($categoria)."</li>";
                include("inc/".$categoriaSemAcento."/".$categoriaSemAcento."-sub-menu.php"); 
            };
        ?>
    </ul>
</div>




 <script>
    const listItem = document.querySelectorAll(".list-group > li");
    const addClassList = (item) => 
    {
        item.classList.add("list-group-item");
    };

    for (let i = 0; i < listItem.length; i++) 
    {
        addClassList(listItem[i]);
    };
</script>

<? include('inc/footer.php'); ?>