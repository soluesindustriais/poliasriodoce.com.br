				 
$(document).ready(function(){
	$('.gallery__main').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.gallery__nav'
	});
	$('.gallery__nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.gallery__main',
		centerMode: true,
		focusOnSelect: true
	});
	$('.carrousel-2').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		centerMode: true,
		focusOnSelect: true,
		fade: true
		
	

	});
});
 
	