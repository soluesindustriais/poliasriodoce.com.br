
function imgAlternateProduct(src) {
    document.querySelector('.imagem-principal-anuncio img').src = src;

    document.querySelector('.ancora-lightbox').href = src;
}

document.querySelectorAll('.img-box').forEach(img => {
    img.addEventListener('mouseover', function () {
        var img = this.querySelector('img');
        imgAlternateProduct(img.src);
    });
});