<?php
/* $minisite = "sansei-talhas";
$subdominio = "sansei-talhas";
$linkminisite = "inc/$minisite/";
$minisite = "";
$linkminisite = ""; */

$linkminisitenb = substr($linkminisite, 0, -1);
$clienteAtivo = "ativo";

$h1 = "Talhas elétricas de cabo de aço";
$title = "Talhas elétricas de cabo de aço";
$desc = "Descubra a eficiência e durabilidade das talhas elétricas de cabo de aço para operações industriais. Explore modelos variados que garantem segurança e robustez em levantamento de cargas pesadas.";
$key = "mpi,sample,lorem,ipsum";
$legendaImagem = "Foto ilustrativa de Armário de aço tipo roupeiro preço";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$nomeurl = pathinfo(basename($_SERVER["PHP_SELF"]), PATHINFO_FILENAME);
include ("$linkminisite"."inc/head-mpi.php");
include ("$linkminisite"."inc/fancy.php");
?>
<style>
  body {
    scroll-behavior: smooth;
  }
  <?
  include('css/header-script.css');
  include ("$linkminisite"."css/mpi-product.css");
  include ("$linkminisite"."css/mpi.css");
  include ("$linkminisite"."css/aside.css");
  include ("$linkminisite"."css/style-mpi.css");
  ?>
</style>



</head>

<body>
  <? include ("$linkminisite"."inc/header-dinamic.php"); ?>
  <main>
    <div class="content" itemscope itemtype="https://schema.org/Article">
      <?= $caminhocateg ?>
      <div class="main-tag-content">
        <div class="container-main-produtos">
          <section class="anuncio-principal wrapper flex-wrap-mobile light-box-shadow">
            <section class="content-anuncio-principal">
              <div class="imagens-anuncio">
                <?php include ("$linkminisite"."inc/gallery-product.php") ?>
              </div>
              <div class="description">
                <hr class="mobile-line">
                <div class="article-content">
                  <div class="ReadMore"
                    style="overflow: hidden; height: auto; transition: height 100ms ease-in-out 0s;">
                    <h2 class="h2-description">Descrição</h2>
                    <p class="p-description">
                      <?= $conteudoPagina[0] ?>
                    </p>
                    <span id="readmore-open">Continuar Lendo <i class="fa-solid fa-turn-down"
                        style="color: var(--azul-solucs);"></i></span>
                    <span id="readmore-close">Fechar <i class="fa-solid fa-turn-up"
                        style="color: var(--azul-solucs);"></i></span>
                  </div>
                </div>
              </div>
            </section>
            <aside class="especificacoes-anuncio-principal">
              <h1 class="h1-title-anuncio">
                <?= $title ?>
              </h1>
              <p class="p-nivel-de-procura">
                Nivel de Procura:
                <span id="span-nivel-de-procura">alta</span>
              </p>

              <div class="espec-tecnicas">
                <h2 class="h2-espec-tecnicas">Especificações Técnicas</h2>
                <ul class="ul-espec-tecnicas">
                  <a href="inc/<?= $minisite ?>/inc/arquivosdownload/catalogo-sansei.pdf"
                    download="catalogo-sansei.pdf">Baixar Catálogo</a>
                  <!-- <a href="/caminho/do/seu/arquivo.pdf" download="NomeDoArquivoParaDownload.pdf">Baixar Arquivo</a> -->
                </ul>
              </div>
              <button class="botao-cotar botao-cotar-mobile btn-solicitar-orcamento btn-cotar" title="<?= $h1 ?>">Solicite um
                Orçamento</button>
              <? include "$linkminisite"."inc/btn-cotar.php"; ?>
              <div class="icons-anuncio">
                <div class="icon-aside">
                  <img src="<?=$linkminisite?>imagens/icons/regiao-atendimento.png"
                    alt=" ICONE REGIÃO DE ATENDIMENTO" />
                  <div class="text-icon">
                    <h2>Região de Atendimento</h2>
                    <p>São Paulo</p>
                  </div>
                </div>
                <div class="icon-categoria icon-aside">
                  <img src="<?=$linkminisite?>imagens/icons/icon-categoria.png" alt=" ICONE CATEGORIA" />
                  <div class="text-icon">
                    <h2>Categoria</h2>
                    <p>Produtos</p>
                  </div>
                </div>
                <div class="icon-sub-categoria icon-aside">
                  <img src="<?=$linkminisite?>imagens/icons/sub-categoria.png" alt=" ICONE CATEGORIA" />
                  <div class="text-icon">
                    <h2>Sub Categoria</h2>
                    <p>
                      <?= $nomeurl ?>
                    </p>
                  </div>
                </div>
              </div>
            </aside>
          </section>
          <div class="wrapper content-produtos">
            <? include "$linkminisite"."inc/produtos-populares.php"; ?>
<!--             <? include "$linkminisite"."inc/produtos-relacionados.php"; ?> -->
            <? include "$linkminisite"."inc/aside-produtos.php"; ?>
            <? include "$linkminisite"."inc/regioes.php"; ?>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <script src="<?= $linkminisite ?>js/img-alternate-product.js"></script>
  <? include "$linkminisite"."inc/footer.php"; ?>
  <script src="<?=$linkminisite?>js/organictabs.jquery.js" async></script>
</body>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    // Seleciona todos os elementos com a classe 'p-side'
    const pSideElements = document.querySelectorAll('.p-side');

    pSideElements.forEach(element => {
      // Adiciona um ouvinte de evento de clique a cada elemento 'p-side'
      element.addEventListener('click', function () {
        // Primeiro, remove a classe 'p-side-active' de todos os elementos 'p-side'
        pSideElements.forEach(el => {
          el.classList.remove('p-side-active');
        });

        // Adiciona a classe 'p-side-active' ao elemento clicado
        element.classList.add('p-side-active');
      });
    });
  });
</script>

<script>
      // Capturar os elementos
  const btnCotar = document.querySelector('.btn-cotar');
  const modal = document.querySelector('.modal-btn');
  const closeBtn = document.querySelector('.modal-btn .close-btn');

  // Função para abrir o modal
  btnCotar.addEventListener('click', () => {
    modal.style.display = 'flex';
  });

  // Função para fechar o modal
  closeBtn.addEventListener('click', () => {
    modal.style.display = 'none';
  });

  // Fechar o modal ao clicar fora dele
  window.addEventListener('click', (e) => {
    if (e.target === modal) {
      modal.style.display = 'none';
    }
  });
</script>


</html>