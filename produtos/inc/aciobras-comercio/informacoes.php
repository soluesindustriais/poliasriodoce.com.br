<? $h1 = "Informações";
$title  = "Informações";
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("inc/head.php");
include("inc/informacoes/informacoes-vetPalavras.php"); ?>

<style>
  body {
    scroll-behavior: smooth;
  }

  <?
  include('css/header-script.css');
  include("$linkminisite" . "css/style.css");
  ?>
</style>
</head>

<body> <? include("inc/header-dinamic.php"); ?><main role="main">
    <section> <?= $caminho ?> <div class="wrapper-produtos"> <br class="clear">
        <h1 style="text-align: center;  "><?= $h1 ?></h1>
        <article class="full">
          <div class="article-content">

            <strong>EMPRESA</strong>
            <p>Desde sua fundação em 1991, a Aciobras vem construindo uma sólida imagem de profissionalismo, de comprometimento com a qualidade e com a satisfação do seu cliente. Sempre acompanhando as evoluções tecnológicas, vem adaptando-se às necessidades de seus clientes em tecnologia, qualidade e confiabilidade.</p>
            
            <strong>MISSÃO</strong>
            <p>Garantir a plena satisfação de nossos clientes com produtos de qualidade,pontualidade e atendimento exemplar. Manter a fidelidade de nossos colaboradores através do reconhecimento e da valorização do seu trabalho.</p>

          </div>
          <ul class="thumbnails-main"> <?php include_once("inc/informacoes/informacoes-categoria.php"); ?> </ul>
        </article>
    </section>
  </main>
  </div>
  <!-- .wrapper --> <? include("inc/footer.php"); ?> </body>

</html>