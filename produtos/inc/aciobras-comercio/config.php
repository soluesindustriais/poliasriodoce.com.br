<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Aciobras Comercio';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Comprometida com a segurança';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '', $tituloCliente);


//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";

// Conteudo das páginas
$conteudoPagina = [
"<strong>Acoplamento de Borracha Flexível: Versatilidade e Redução de Vibrações</strong><br>
O <strong>acoplamento de borracha flexível</strong> é um componente essencial para conectar eixos de máquinas, proporcionando flexibilidade e reduzindo vibrações e desalinhamentos. Utilizados em diversas aplicações industriais, esses acoplamentos melhoram a eficiência e prolongam a vida útil dos equipamentos.<br>
<strong>Vantagens do Acoplamento de Borracha Flexível</strong><br>
A principal vantagem do acoplamento de borracha flexível é sua capacidade de absorver choques e vibrações, protegendo os componentes conectados e minimizando o desgaste. Além disso, esses acoplamentos compensam desalinhamentos axiais, radiais e angulares, garantindo um funcionamento suave e eficiente das máquinas.<br>
<strong>Aplicações do Acoplamento de Borracha Flexível</strong><br>
Os acoplamentos de borracha flexível são amplamente utilizados em bombas, compressores, ventiladores, motores e outros equipamentos industriais. Sua flexibilidade os torna ideais para situações onde há necessidade de absorção de choques e vibrações, proporcionando uma operação mais silenciosa e suave. Eles são especialmente úteis em ambientes onde a redução de ruído é crucial.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento de borracha flexível</strong> inclui inspeções regulares para verificar a integridade da borracha e a presença de desgaste ou rachaduras. Substituições periódicas são necessárias para garantir o desempenho contínuo e evitar falhas que possam levar a paradas não planejadas. Lubrificação adequada e alinhamento correto também são essenciais para prolongar a vida útil do acoplamento.<br>
<strong>Dicas para Escolher um Acoplamento de Borracha Flexível</strong><br>
Ao selecionar um acoplamento de borracha flexível, considere a carga e a velocidade de operação, o tipo de desalinhamento a ser compensado e o ambiente de operação. Escolha um modelo que ofereça a combinação certa de flexibilidade e resistência para suas necessidades específicas. Certifique-se de seguir as especificações do fabricante para instalação e manutenção.<br>
Invista em acoplamentos de borracha flexível para garantir a operação eficiente e prolongar a vida útil de seus equipamentos industriais, reduzindo vibrações e minimizando o desgaste.",
"<strong>Acoplamento de Borracha para Motor: Desempenho e Durabilidade</strong><br>
O <strong>acoplamento de borracha para motor</strong> é um componente crucial para a conexão de eixos, oferecendo flexibilidade e absorção de vibrações. Utilizados amplamente em motores industriais e automotivos, esses acoplamentos ajudam a melhorar o desempenho e a longevidade dos sistemas.<br>
<strong>Vantagens do Acoplamento de Borracha para Motor</strong><br>
A principal vantagem do acoplamento de borracha é sua capacidade de absorver choques e vibrações, protegendo os componentes conectados e reduzindo o desgaste. Além disso, ele compensa desalinhamentos leves entre os eixos, o que é essencial para manter o funcionamento suave e eficiente do motor. Isso resulta em menor manutenção e maior durabilidade dos equipamentos.<br>
<strong>Aplicações do Acoplamento de Borracha para Motor</strong><br>
Esses acoplamentos são utilizados em uma ampla gama de aplicações, incluindo motores elétricos, motores de combustão interna, compressores, bombas e ventiladores. Sua flexibilidade os torna ideais para situações onde há necessidade de absorção de choques e vibrações, proporcionando uma operação mais silenciosa e suave. Eles são especialmente úteis em ambientes industriais e automotivos, onde a confiabilidade é fundamental.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento de borracha para motor</strong> inclui inspeções regulares para detectar sinais de desgaste, rachaduras ou deformações na borracha. É importante substituir o acoplamento quando necessário para evitar falhas que possam causar paradas não planejadas. Além disso, a lubrificação adequada e o alinhamento correto dos eixos são cruciais para garantir o desempenho ideal e prolongar a vida útil do acoplamento.<br>
<strong>Dicas para Escolher um Acoplamento de Borracha para Motor</strong><br>
Ao escolher um acoplamento de borracha para motor, considere a carga e a velocidade de operação, o tipo de desalinhamento a ser compensado e as condições ambientais. Escolha um modelo que ofereça a combinação certa de flexibilidade e resistência para suas necessidades específicas. Siga as recomendações do fabricante para a instalação e manutenção para garantir a máxima eficiência e durabilidade.<br>
Invista em acoplamentos de borracha para motor para garantir um desempenho eficiente e duradouro, reduzindo vibrações e minimizando o desgaste dos seus sistemas motorizados.",
"<strong>Acoplamento de Eixo de Motor: Conexão e Desempenho Ótimos</strong><br>
O <strong>acoplamento de eixo de motor</strong> é essencial para conectar eixos de motores a diversos tipos de equipamentos, garantindo uma transferência de torque eficiente e minimizando desgastes. Utilizados em uma vasta gama de aplicações industriais e automotivas, esses acoplamentos asseguram a operação suave e a longevidade dos sistemas.<br>
<strong>Vantagens do Acoplamento de Eixo de Motor</strong><br>
A principal vantagem dos acoplamentos de eixo é sua capacidade de absorver desalinhamentos leves, choques e vibrações, protegendo os componentes conectados e prolongando a vida útil do sistema. Eles também ajudam a manter a precisão do alinhamento dos eixos, o que é crucial para o funcionamento eficiente dos motores e dos equipamentos acoplados.<br>
<strong>Aplicações do Acoplamento de Eixo de Motor</strong><br>
Os acoplamentos de eixo de motor são amplamente utilizados em motores elétricos, motores de combustão interna, bombas, compressores, geradores e outros equipamentos industriais. Sua versatilidade os torna ideais para situações onde a redução de vibrações e a compensação de desalinhamentos são necessárias, garantindo a eficiência operacional e a proteção dos sistemas.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento de eixo de motor</strong> envolve inspeções regulares para verificar a integridade do acoplamento, incluindo a detecção de desgaste, rachaduras ou deformações. É essencial substituir os acoplamentos desgastados para evitar falhas e paradas não planejadas. Além disso, a lubrificação adequada e o alinhamento preciso dos eixos são fundamentais para assegurar o desempenho ideal e a longevidade do acoplamento.<br>
<strong>Dicas para Escolher um Acoplamento de Eixo de Motor</strong><br>
Ao escolher um acoplamento de eixo de motor, considere a carga e a velocidade de operação, o tipo de desalinhamento a ser compensado e as condições ambientais. Opte por modelos que ofereçam a combinação certa de flexibilidade e resistência para suas necessidades específicas. Siga as recomendações do fabricante para instalação e manutenção para garantir a máxima eficiência e durabilidade do acoplamento.<br>
Invista em acoplamentos de eixo de motor para garantir uma operação eficiente e duradoura, reduzindo vibrações e minimizando o desgaste dos seus sistemas motorizados.",
"<strong>Acoplamento Elástico para Encoder: Precisão e Flexibilidade</strong><br>
O <strong>acoplamento elástico para encoder</strong> é um componente essencial para garantir a precisão na transmissão de movimentos entre o eixo do motor e o encoder. Projetado para absorver choques, vibrações e desalinhamentos leves, este tipo de acoplamento melhora a eficiência e a durabilidade dos sistemas de medição de posição e velocidade.<br>
<strong>Vantagens do Acoplamento Elástico para Encoder</strong><br>
A principal vantagem do acoplamento elástico é sua capacidade de compensar desalinhamentos axiais, radiais e angulares, proporcionando uma transmissão de movimento precisa e suave. Além disso, ele absorve vibrações e choques, protegendo tanto o encoder quanto o motor, o que resulta em menor desgaste e maior vida útil dos componentes.<br>
<strong>Aplicações do Acoplamento Elástico para Encoder</strong><br>
Os acoplamentos elásticos para encoders são amplamente utilizados em sistemas de automação, robótica, máquinas CNC e equipamentos de medição. Sua flexibilidade e precisão tornam-nos ideais para aplicações onde é necessário manter a exatidão das medições de posição e velocidade, mesmo em condições de operação adversas.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento elástico para encoder</strong> é simples, envolvendo inspeções regulares para detectar sinais de desgaste, rachaduras ou deformações no material elástico. Substituições periódicas são recomendadas para garantir a continuidade da precisão e a proteção dos componentes. Além disso, a verificação do alinhamento dos eixos e a limpeza adequada são essenciais para manter o desempenho ideal.<br>
<strong>Dicas para Escolher um Acoplamento Elástico para Encoder</strong><br>
Ao escolher um acoplamento elástico para encoder, considere a capacidade de torque, a precisão necessária, o tipo de desalinhamento a ser compensado e as condições ambientais de operação. Opte por modelos de alta qualidade que ofereçam a combinação certa de flexibilidade e resistência para suas necessidades específicas. Siga as instruções do fabricante para a instalação e manutenção adequadas.<br>
Invista em acoplamentos elásticos para encoder para garantir medições precisas e a proteção dos seus sistemas de automação, prolongando a vida útil dos componentes e melhorando a eficiência operacional.",
"<strong>Acoplamento Estriado: Conexão Segura e Eficiente</strong><br>
O <strong>acoplamento estriado</strong> é uma solução robusta e confiável para conectar eixos, garantindo uma transmissão de torque eficiente e segura. Utilizado em diversas aplicações industriais, este tipo de acoplamento é ideal para situações que exigem alta precisão e resistência ao desgaste.<br>
<strong>Vantagens do Acoplamento Estriado</strong><br>
A principal vantagem do acoplamento estriado é sua capacidade de transmitir altos torques com precisão, graças ao design de estrias que proporciona um encaixe firme entre os eixos. Esse tipo de acoplamento também permite uma certa flexibilidade axial, o que ajuda a acomodar desalinhamentos leves e a absorver choques, aumentando a durabilidade dos componentes conectados.<br>
<strong>Aplicações do Acoplamento Estriado</strong><br>
Os acoplamentos estriados são amplamente utilizados em máquinas-ferramenta, equipamentos agrícolas, veículos automotivos, e em várias aplicações industriais que requerem uma conexão robusta e eficiente entre eixos. Eles são especialmente úteis em situações onde a transmissão de potência e a resistência ao desgaste são críticas para o desempenho do sistema.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento estriado</strong> inclui inspeções regulares para verificar o estado das estrias e detectar sinais de desgaste ou deformações. Lubrificação adequada e ajustes periódicos são essenciais para garantir o alinhamento correto e prevenir folgas que possam comprometer a eficiência da transmissão de torque. Substituir o acoplamento quando necessário ajuda a evitar falhas e a manter a operação contínua.<br>
<strong>Dicas para Escolher um Acoplamento Estriado</strong><br>
Ao escolher um acoplamento estriado, considere a capacidade de torque, o tipo de material, a precisão necessária e as condições ambientais de operação. Opte por modelos que ofereçam alta resistência e durabilidade, adequados para suas necessidades específicas. Siga as instruções do fabricante para a instalação e manutenção corretas, garantindo o máximo desempenho e longevidade do acoplamento.<br>
Invista em acoplamentos estriados para assegurar uma conexão segura e eficiente entre eixos, melhorando a confiabilidade e a durabilidade dos seus sistemas industriais.",
"<strong>Acoplamento Flangeado: Conexão Robusta e Segura</strong><br>
O <strong>acoplamento flangeado</strong> é uma solução robusta para conectar eixos de maneira segura e eficiente, sendo amplamente utilizado em diversas aplicações industriais. Este tipo de acoplamento oferece alta resistência e durabilidade, garantindo uma transmissão de torque confiável e estável.<br>
<strong>Vantagens do Acoplamento Flangeado</strong><br>
A principal vantagem do acoplamento flangeado é sua capacidade de transmitir altos torques com segurança, graças ao seu design que proporciona uma conexão firme entre os eixos. Além disso, ele permite fácil desmontagem e montagem, facilitando a manutenção e o ajuste dos componentes conectados. A robustez do acoplamento flangeado o torna ideal para aplicações que exigem alta precisão e resistência ao desgaste.<br>
<strong>Aplicações do Acoplamento Flangeado</strong><br>
Os acoplamentos flangeados são amplamente utilizados em setores como a indústria naval, petroquímica, siderúrgica, mineração e em equipamentos de transporte pesado. Eles são ideais para conectar eixos de motores, bombas, compressores e outros equipamentos industriais que requerem uma conexão sólida e confiável.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento flangeado</strong> envolve inspeções regulares para verificar o estado dos flanges e parafusos, garantindo que estejam apertados e livres de desgaste ou corrosão. A lubrificação adequada e a verificação do alinhamento dos eixos são cruciais para manter o desempenho ideal e prolongar a vida útil do acoplamento. Substituir componentes desgastados ou danificados prontamente ajuda a evitar falhas e a garantir a operação contínua.<br>
<strong>Dicas para Escolher um Acoplamento Flangeado</strong><br>
Ao selecionar um acoplamento flangeado, considere a capacidade de torque, o material de construção, a compatibilidade com os eixos e as condições operacionais. Escolha modelos de alta qualidade que ofereçam resistência e durabilidade adequadas às suas necessidades específicas. Siga as recomendações do fabricante para a instalação e manutenção corretas, garantindo o máximo desempenho e segurança.<br>
Invista em acoplamentos flangeados para assegurar conexões robustas e seguras entre eixos, melhorando a confiabilidade e a eficiência dos seus sistemas industriais.",
"<strong>Acoplamento Flexível de Engrenagem: Desempenho e Flexibilidade</strong><br>
O <strong>acoplamento flexível de engrenagem</strong> é uma solução eficaz para conectar eixos, oferecendo flexibilidade e alta capacidade de torque. Este tipo de acoplamento é amplamente utilizado em aplicações industriais que exigem absorção de desalinhamentos e minimização de vibrações, garantindo um desempenho confiável e duradouro.<br>
<strong>Vantagens do Acoplamento Flexível de Engrenagem</strong><br>
A principal vantagem do acoplamento flexível de engrenagem é sua capacidade de compensar desalinhamentos axiais, radiais e angulares, proporcionando uma transmissão de torque suave e eficiente. Além disso, ele absorve choques e vibrações, protegendo os componentes conectados e reduzindo o desgaste. Este tipo de acoplamento também é conhecido por sua durabilidade e resistência em ambientes industriais rigorosos.<br>
<strong>Aplicações do Acoplamento Flexível de Engrenagem</strong><br>
Os acoplamentos flexíveis de engrenagem são amplamente utilizados em indústrias como a metalúrgica, petroquímica, mineração, papel e celulose, e em equipamentos de transporte pesado. Eles são ideais para conectar eixos de motores, redutores, bombas e compressores, onde a flexibilidade e a capacidade de lidar com desalinhamentos são essenciais para a operação eficiente.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento flexível de engrenagem</strong> inclui inspeções regulares para verificar o estado das engrenagens e das áreas de contato, garantindo que estejam livres de desgaste excessivo e devidamente lubrificadas. Verificar o alinhamento dos eixos e apertar os parafusos de fixação são passos importantes para assegurar o desempenho ideal e prolongar a vida útil do acoplamento. Substituições periódicas das partes desgastadas ajudam a evitar falhas inesperadas.<br>
<strong>Dicas para Escolher um Acoplamento Flexível de Engrenagem</strong><br>
Ao escolher um acoplamento flexível de engrenagem, considere a capacidade de torque, o tipo de desalinhamento a ser compensado, as condições operacionais e o material de construção. Opte por modelos de alta qualidade que ofereçam a resistência e a durabilidade necessárias para suas aplicações específicas. Siga as recomendações do fabricante para a instalação e manutenção corretas, garantindo o máximo desempenho e segurança.<br>
Invista em acoplamentos flexíveis de engrenagem para garantir conexões eficientes e duradouras entre eixos, melhorando a confiabilidade e a eficiência dos seus sistemas industriais.",
"<strong>Acoplamento para Bomba: Conexão Segura e Eficiente</strong><br>
O <strong>acoplamento para bomba</strong> é um componente essencial para garantir a conexão segura e eficiente entre o motor e a bomba, permitindo uma transferência de torque suave e minimizando vibrações e desalinhamentos. Utilizado em diversas aplicações industriais e comerciais, este tipo de acoplamento assegura o desempenho confiável e duradouro do sistema.<br>
<strong>Vantagens do Acoplamento para Bomba</strong><br>
A principal vantagem de um acoplamento para bomba é sua capacidade de absorver choques e vibrações, protegendo os componentes conectados e reduzindo o desgaste. Além disso, ele compensa desalinhamentos leves entre o motor e a bomba, garantindo uma operação suave e eficiente. Isso resulta em menor necessidade de manutenção e maior durabilidade do equipamento.<br>
<strong>Aplicações do Acoplamento para Bomba</strong><br>
Os acoplamentos para bombas são amplamente utilizados em sistemas de bombeamento de água, óleo, produtos químicos e outros líquidos em indústrias como a petroquímica, mineração, agricultura, tratamento de água e esgoto, e HVAC. Sua flexibilidade os torna ideais para aplicações onde a absorção de choques e vibrações é necessária para manter a eficiência operacional e proteger os componentes.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento para bomba</strong> inclui inspeções regulares para detectar sinais de desgaste, rachaduras ou deformações. É essencial substituir o acoplamento quando necessário para evitar falhas e paradas não planejadas. Além disso, a lubrificação adequada e o alinhamento correto dos eixos são cruciais para garantir o desempenho ideal e prolongar a vida útil do acoplamento.<br>
<strong>Dicas para Escolher um Acoplamento para Bomba</strong><br>
Ao escolher um acoplamento para bomba, considere a capacidade de torque, a velocidade de operação, o tipo de desalinhamento a ser compensado e as condições ambientais. Opte por modelos que ofereçam a combinação certa de flexibilidade e resistência para suas necessidades específicas. Siga as recomendações do fabricante para a instalação e manutenção adequadas, garantindo a máxima eficiência e durabilidade.<br>
Invista em acoplamentos para bombas para assegurar a operação eficiente e duradoura dos seus sistemas de bombeamento, reduzindo vibrações e minimizando o desgaste dos componentes.",
"<strong>Cruzeta de Borracha para Acoplamento: Flexibilidade e Durabilidade</strong><br>
A <strong>cruzeta de borracha para acoplamento</strong> é um componente vital que proporciona flexibilidade e absorção de vibrações em sistemas de transmissão de torque. Utilizada em diversos tipos de acoplamentos, a cruzeta de borracha ajuda a proteger os componentes conectados, garantindo um desempenho suave e eficiente.<br>
<strong>Vantagens da Cruzeta de Borracha para Acoplamento</strong><br>
A principal vantagem da cruzeta de borracha é sua capacidade de absorver choques e vibrações, reduzindo o desgaste e prolongando a vida útil dos componentes do sistema. Ela também compensa desalinhamentos axiais, radiais e angulares, proporcionando uma transmissão de torque confiável e estável. Além disso, a borracha oferece resistência a óleos, graxas e outras substâncias químicas, garantindo durabilidade em ambientes industriais rigorosos.<br>
<strong>Aplicações da Cruzeta de Borracha para Acoplamento</strong><br>
As cruzetas de borracha são amplamente utilizadas em acoplamentos de motores, bombas, compressores, ventiladores e outros equipamentos industriais. Sua flexibilidade e resistência as tornam ideais para aplicações que exigem a absorção de vibrações e a compensação de desalinhamentos, garantindo a operação eficiente e protegendo os componentes do sistema.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de uma <strong>cruzeta de borracha para acoplamento</strong> inclui inspeções regulares para verificar o estado da borracha, procurando por sinais de desgaste, rachaduras ou deformações. É importante substituir a cruzeta de borracha quando necessário para evitar falhas e manter a eficiência do sistema. Além disso, verificar o alinhamento dos eixos e a lubrificação adequada dos componentes ajuda a prolongar a vida útil da cruzeta.<br>
<strong>Dicas para Escolher uma Cruzeta de Borracha para Acoplamento</strong><br>
Ao escolher uma cruzeta de borracha para acoplamento, considere a capacidade de torque, o tipo de desalinhamento a ser compensado, as condições ambientais e a compatibilidade com o acoplamento. Opte por cruzetas de alta qualidade que ofereçam a resistência e durabilidade necessárias para suas aplicações específicas. Siga as recomendações do fabricante para instalação e manutenção para garantir o máximo desempenho e segurança.<br>
Invista em cruzetas de borracha para acoplamento para assegurar flexibilidade, durabilidade e eficiência em seus sistemas de transmissão de torque, protegendo e prolongando a vida útil dos componentes conectados.",
"<strong>Acoplamento Tipo Cruzeta: Flexibilidade e Desempenho</strong><br>
O <strong>acoplamento tipo cruzeta</strong> é uma solução eficiente para conectar eixos, oferecendo flexibilidade e alta capacidade de absorção de vibrações. Este tipo de acoplamento é amplamente utilizado em diversas aplicações industriais, garantindo uma transmissão de torque suave e protegendo os componentes conectados.<br>
<strong>Vantagens do Acoplamento Tipo Cruzeta</strong><br>
A principal vantagem do acoplamento tipo cruzeta é sua capacidade de compensar desalinhamentos axiais, radiais e angulares, proporcionando uma transmissão de torque confiável e estável. Além disso, ele absorve choques e vibrações, reduzindo o desgaste e prolongando a vida útil dos componentes do sistema. A estrutura de cruzeta permite uma flexibilidade superior, ideal para aplicações que requerem movimento e ajuste constante.<br>
<strong>Aplicações do Acoplamento Tipo Cruzeta</strong><br>
Os acoplamentos tipo cruzeta são utilizados em uma ampla gama de indústrias, incluindo a automotiva, agrícola, metalúrgica e petroquímica. Eles são ideais para conectar eixos de motores, bombas, compressores e outros equipamentos onde a absorção de vibrações e a compensação de desalinhamentos são cruciais para a operação eficiente. Estes acoplamentos são particularmente úteis em máquinas que operam sob condições de carga variável e movimentos complexos.<br>
<strong>Manutenção e Cuidados</strong><br>
A manutenção de um <strong>acoplamento tipo cruzeta</strong> inclui inspeções regulares para verificar o estado das cruzetas e das áreas de contato, garantindo que estejam livres de desgaste excessivo e devidamente lubrificadas. Verificar o alinhamento dos eixos e apertar os parafusos de fixação são passos importantes para assegurar o desempenho ideal e prolongar a vida útil do acoplamento. Substituições periódicas das partes desgastadas ajudam a evitar falhas inesperadas.<br>
<strong>Dicas para Escolher um Acoplamento Tipo Cruzeta</strong><br>
Ao escolher um acoplamento tipo cruzeta, considere a capacidade de torque, o tipo de desalinhamento a ser compensado, as condições operacionais e o material de construção. Opte por modelos de alta qualidade que ofereçam a resistência e a durabilidade necessárias para suas aplicações específicas. Siga as recomendações do fabricante para a instalação e manutenção corretas, garantindo o máximo desempenho e segurança.<br>
Invista em acoplamentos tipo cruzeta para garantir conexões eficientes e duradouras entre eixos, melhorando a confiabilidade e a eficiência dos seus sistemas industriais.",
];


// Criação de página
// Criação Mini site


// Criar página de produto mpi
$VetPalavrasProdutos = [
    "acoplamento-de-borracha-flexivel",
    "acoplamento-de-borracha-para-motor",
    "acoplamento-de-eixo-de-motor",
    "acoplamento-elastico-para-encoder",
    "acoplamento-estriado",
    "acoplamento-flangeado",
    "acoplamento-flexivel-engrenagem",
    "acoplamento-para-bomba",
    "cruzeta-de-borracha-para-acoplamento",
    "acoplamento-tipo-cruzeta"
];

//Criar página de Serviço
$VetPalavrasInformacoes = [];



// Numero do formulario de cotação
$formCotar = 171;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Aciobras Comercio';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'A melhor opção do seguimento';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Rua Fortunato Ferraz, 896';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Vila Anastácio';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'São Paulo';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'SP';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '05093-000';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-fixo.svg';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'comercial@ventair.com.br';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : 'https://www.instagram.com/depositomineiro/';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';



?>


<!-- VARIAVEIS DE CORES -->

<style>
    :root {
        --azul-solucs: #2d2d2f;
        --second-color: #012f73;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>