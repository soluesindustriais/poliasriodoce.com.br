<section class="wrapper">
    <div class="section-text">
        <h2 class="text-center">Great Features</h2>
        <div class="d-flex content-feature flex-wrap-mobile">
            <div class="grid-features">
                <div class="grid-item"><img src="imagens/banner/site-base-banner-2.png" alt="img"></div>

                <div class="grid-item p-5">
                <i class="fa-solid fa-share-from-square"></i>
                    <h4>headline</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem dolorum totam iusto adipisci dolore consequatur</p>                    
                    <a href="<?= $url ?>duda-godoi" title="duda godoi" style="background-color: var(--color-secundary); color: #fff; padding: 10px 60px;">CTA</a>
                </div>

                <div class="grid-item"><img src="imagens/banner/site-base-banner-2.png" alt="img"></div>

                <div class="grid-item p-5">
                <i class="fa-solid fa-arrow-trend-up"></i>
                    <h4>headline</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem dolorum totam iusto adipisci dolore consequatur</p>
                <a href="<?= $url ?>duda-godoi" title="duda godoi" style="background-color: var(--color-secundary); color: #fff; padding: 10px 60px;">CTA</a>
                </div>

                <div class="grid-item"><img src="imagens/banner/site-base-banner-2.png" alt="img"></div>

                <div class="grid-item p-5">
                <i class="fa-solid fa-chart-simple"></i>
                    <h4>headline</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem dolorum totam iusto adipisci dolore consequatur</p>
                <a href="<?= $url ?>duda-godoi" title="duda godoi" style="background-color: var(--color-secundary); color: #fff; padding: 10px 60px;">CTA</a>
                </div>
            </div>
        </div>
    </div>
</section>