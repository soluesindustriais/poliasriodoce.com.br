<?php

        include 'inc/vetCategorias.php';
        foreach ($vetCategorias as $categoria) {
      
        $nomeDaCategoriaSemAcentoComHifen = remove_acentos($categoria);
        
        // Substituindo hifens por espaços e depois aplicando ucwords
        $NomeDaCategoriaUpper = ucwords(str_replace('-', ' ', $categoria));

        echo "<li><a href=\"" . $url . $nomeDaCategoriaSemAcentoComHifen . "-categoria\" title=\"Categoria - " . $NomeDaCategoriaUpper . "\">Categoria - " . $NomeDaCategoriaUpper . "</a></li>\n";
        }
        ?>