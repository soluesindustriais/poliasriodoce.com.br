<section class="feedback">
    <h4 class="sub-title">Provas Sociais</h4>
    <h2 class="text-center title">Feedbacks de Clientes</h2>
    <div class="swiper wrapper">
        <div class="swiper-wrapper">
            <!-- Slides -->
            <div class="swiper-slide">
                <div class="slide-feedback">
                    <i class="fa-regular fa-comments"></i>
                    <h2>"Nós estamos sentido uma diferença no número de vendas."</h2>
                    <img src="imagens/clientes/junseal.png" alt="imagem feedback">
                    <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                    <div class="user">
                        <h3>MAURO SANCHEZ</h3>
                        <h3>Brazil</h3>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="slide-feedback">
                    <i class="fa-regular fa-comments"></i>
                    <h2>"Temos retorno com o Soluções Industriais, novos contatos comerciais e novas prospecções."</h2>
                    <img src="imagens/clientes/maze.png" alt="imagem feedback">
                    <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                    <div class="user">
                        <h3>SÉRGIO THOMAZELLI</h3>
                        <h3>Brazil</h3>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="slide-feedback">
                    <i class="fa-regular fa-comments"></i>
                    <h2>"O Soluções Industriais vêm somando para o crescimento da nossa empresa."</h2>
                    <img src="imagens/clientes/itaqua.png" alt="imagem feedback">
                    <span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
                    <div class="user">
                        <h3>CLÉCIO ARAÚJO</h3>
                        <h3>Brazil</h3>
                    </div>
                </div>
            </div>

        </div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
</section>

<script>
    const swiper = new Swiper('.swiper', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,


        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    });
</script>