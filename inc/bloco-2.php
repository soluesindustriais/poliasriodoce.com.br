

<!-- Galeria dinâmica start -->
<div class="mt-5">
<div class="container p-0 my-3">
   <div class="row justify-content-center py-3">
      <div class="col-lg-12 pt-2 mt-2 text-center">
         <div id="paginas-destaque" class="owl-carousel owl-theme owl-loaded owl-drag">
            <div class="col-12 py-2">
               <div class="owl-stage-outer">
                  <div class="owl-stage">
                     <? 
                     $j = 0;
                     for($i=0;$i<12;$i++): 
                     $categoriaSemAcento = $trata->trataAcentos($categorias->getCategorias()[$j]);
                     $categoriaSemHifen = $trata->retiraHifen($categorias->getCategorias()[$j]);
                     ?>
                     <div class="owl-item">
                        <div class="card blog rounded border-0 shadow overflow-hidden">
                           <div class="position-relative border-intro">
                              <a class="lightbox"
                                 href="<?=$url;?>imagens/<?= $categoriaSemAcento ;?>/<?= $categoriaSemAcento ;?>-<?=$i+1;?>.jpg"
                                 title="<?=$categoriaSemHifen?>">
                                 <img class='lazyload'
                                    data-src="<?=$url;?>imagens/<?= $categoriaSemAcento ;?>/thumbs/<?= $categoriaSemAcento ;?>-<?=$i+1;?>.jpg"
                                    alt="<?=$categoriaSemHifen?>" title="<?=$categoriaSemHifen?>" />
                                 <div class="overlay rounded-top bg-dark">
                                 </div>
                                 <div class="author">
                                    <small class="text-light user d-block"></small>
                                    <small class="text-light date"><?=$categoriaSemHifen?></small>
                                 </div>
                           </div>
                        </div>
                        </a>
                     </div>
                  <? 
                  $j++;
                  if ($j >= count($categorias->getCategorias()))
                  {
                     $j = 0;
                  };
                  endfor; 
                  ?>
                  </div>               
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!-- Galeria dinâmica end -->


      <div class="col-12 mt-5 pt-5 pb-2 text-center">
         <div class="row section-title mb-4">
            <h3 class="display-4 pb-3 col-md-6 col-sm-12">Seja <b class="display-3 text-primary">bem vindo</b> à nova maneira de
               solicitar orçamentos!</h3> <img class='lazyload' data-src="imagens/home/inovacao-solucoes-industriais.svg"
               class="col-md-6 col-sm-12" alt="">
            <p class="h4 text-left mb-4 col-12 mt-4">Aqui você encontra os melhores fabricantes e fornecedores especialistas
               em soluções para o seu negócio e com apenas um orçamento, receba dezenas de contatos!</p>
         </div>
      </div>
   </div>
</div>
</section>
<!-- Section 1 End -->