
<? include('inc/vetDestaque.php'); ?>
<section class="section pt-5">
   <div class="container mt-0 pt-2">

   <h2 class="pt-5 text-center">Encontre as <span
                     class="text-primary font-weight-bold">melhores empresas</span>, tudo em um <span
                     class="text-primary font-weight-bold">único lugar!</span></h2>
   <h2 class="pb-5 text-center mt-5"><span
                     class="text-primary font-weight-bold">Busque</span> facilmente o que procura!</h2>
   <? include( 'inc/search-box.php' ); ?>

<div class="section pb-3 pt-5">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-12 text-center">
            <div class="section-title mb-4 pb-2">
               <h4 class="title mb-4">Conheça nossos produtos</h4>
               <p class="text-muted para-desc mx-auto mb-0">Receba cotação de
               <? 
                  $i = 0; 
                  $numCategorias = count($categorias->getCategorias())-1; 
                  foreach($trata->retiraHifen($categorias->getCategorias()) as $categoria): ?>
               
                  <? if ( $i < $numCategorias ): ?>
                     <span class="text-primary font-weight-bold"><?= $categoria; ?>, </span> 
                  <? else: ?>
                     <span class="text-primary font-weight-bold"><?= $categoria; ?></span> 
                  <? endif; ?>

               <? $i++; endforeach; ?>
               e muito mais. </p>
            </div>
         </div>
         <!--end col-->
      </div>
      <!--end row-->
      <!-- Partners End -->
      <div class="row">
         <div class="col-12 mt-4 pt-2">
            <div id="customer-testi" class="owl-carousel owl-theme owl-loaded owl-drag">
               <div class="owl-stage-outer">
                  <div class="owl-stage">
                     <? foreach($vetDestaque as $palavras => $palavra): ?>
                     <div style="height: 280px; max-height: 100%;" class="owl-item overflow-hidden position-relative products-carousel-home">
                        <a href="<?= $trata->trataAcentos(($palavra['palavra'])); ?>">
                           <div class="card customer-testi border-0 text-center position-relative bg-transparent">
                              <img class="w-100 mw-100 position-absolute absolute-top absolute-left lazyload" data-src="imagens/home/destaque/<?= $trata->trataAcentos(($palavra['palavra'])); ?>.jpg" alt="<?=$trata->retiraHifen($palavra['palavra']);?>">
                              <div class="card-body position-relative overflow-hidden bg-transparent">
                                <!--  <p class="d-none text-dark mt-1"><?=$palavra['texto'];?></p> -->
                                 <h3 class="text-primary text-center mt-3"><?=$trata->retiraHifen($palavra['palavra']);?></h3>
                              </div>
                           </div>
                        </a>
                     </div>
                     <? endforeach; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
     
     
     
     <!-- CARROSSEL COM LOGO INIT
      <div class="row pb-5">
            <div class="container mb-5 py-5">

            <div id="customer-test" class="owl-carousel owl-theme owl-loaded owl-drag">
               <div class="owl-stage-outer">
                  <div class="owl-stage">

                     <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img class='lazyload' data-src="imagens/instalacao-e-montagem-industrial/instalacao-e-montagem-industrial-01.jpg" class="avatar avatar-medium w-100" alt="instalacao-e-montagem">
                           </div>
                        </div>
                     </div>
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img class='lazyload' data-src="imagens/manutencao-predial/manutencao-predial-01.jpg" class="avatar avatar-medium w-100" alt="manutencao-predial">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img class='lazyload' data-src="imagens/painel-e-quadro-eletrico/painel-e-quadro-eletrico-02.jpg" class="avatar avatar-medium w-100" alt="painel-e-quadro-eletrico">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img class='lazyload' data-src="imagens/servicos-para-camaras-frias/servicos-para-camaras-frias-02.jpg" class="avatar avatar-medium w-100" alt="servicos-para-camaras-frias">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img class='lazyload' data-src="imagens/servicos-spda-para-raios/servicos-spda-para-raios-4.jpg" class="avatar avatar-medium w-100" alt="">
                           </div>
                        </div>
                     </div>                      
                  </div>
               </div>
            </div>
                  </div>
   </div> 
      CARROSSEL COM LOGO END-->  



<div class="row">    
<h2 class="col-12 w-100 text-center py-5">Vantagens</h2>
         <div class="col-md-4 col-12">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img  data-src="imagens/svg/user.svg" class="avatar avatar-large lazyload" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Cote com diversas empresas</h4>
                  <p class="text-muted mb-0">Solicite orçamentos com vários fornecedores ao mesmo tempo e em poucos cliques.</p>
               </div>
            </div>
         </div>
         <!--end col-->
         <div class="col-md-4 col-12 mt-5 mt-sm-0">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img  data-src="imagens/svg/calendar.svg" class="avatar avatar-large lazyload" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Dezenas de cotações diariamente</h4>
                  <p class="text-muted mb-0">Solicite orçamento com dezenas de fornecedores qualificados de forma simples.</p>
               </div>
            </div>
         </div>
         <!--end col-->
         <div class="col-md-4 col-12 mt-5 mt-sm-0">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img  data-src="imagens/svg/sand-clock.svg" class="avatar avatar-large lazyload" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Economize tempo</h4>
                  <p class="text-muted mb-0">Otimize seu tempo e selecione os melhores fornecedores.</p>
               </div>
            </div>
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->
   <div class="container mt-100 mt-60">
      <div class="row align-items-center">
         <div class="col-lg-6 col-md-6">
            <img data-src="imagens/home/<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>.jpg" class="img-fluid shadow rounded mw-100 lazyload" alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[0]));?>">
         </div>
         <!--end col-->
         <div class="col-lg-6 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
            <div class="section-title ml-lg-5">
               <h4 class="title mb-4"><?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[0])); ?></h4>
               <p class="text-muted">Os equipamentos de processamento de alimentos são fundamentais para a indústria alimentícia desenvolver com maior qualidade e eficiência o processo de tratamento dos alimentos, desde o início do procedimento de abate até as etapas finais.</p>
               <a href="<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>-categoria" class="mt-3 text-primary">Saiba Mais </a>
            </div>
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->
   <div class="container mt-100 mt-60">
      <div class="row align-items-center">
         <div class="col-lg-7 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
            <div class="section-title mr-lg-5">
               <h4 class="title mb-4"><?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[1])); ?></h4>
               <p class="text-muted">O acoplamento de ferro tem como característica facilitar a união entre eixos alinhados ou levemente desalinhados. Sendo assim, é fundamental que a aquisição seja feita em empresas especializadas, que desenvolve peças para atuar nas funções.</p>
               <a href="<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>-categoria" class="mt-3 text-primary">Saiba Mais</a>
            </div>
         </div>
         <!--end col-->
         <div class="col-lg-5 col-md-6 order-1 order-md-2">
            <img data-src="imagens/home/<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>.jpg" class="img-fluid shadow rounded mw-100 lazyload" alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[1]));?>">
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->
