<!DOCTYPE html>
<html itemscope itemtype="https://schema.org/Thing" lang="pt-br">
<head>
    <meta charset="utf-8">
    <? include('inc/geral.php'); ?>
<!-- Includes Principais -->
<script src="js/jquery-3.4.1.min.js"></script>
<!-- Styles  e lazysizes-->
<!-- lazysizes-->
<script src="js/lazysizes.min.js" async></script>
<? include "inc/fancy.php"?>
<!-- slicknav -->
<script src="hero/js/modernizr.js"></script>
<!-- fontawesome -->
    	<title><?=$h1?> Polias RD</title>
<link rel="preload" href="<?=$url;?>css/style.css" as="style">
<link rel="stylesheet" href="<?=$url;?>css/style.css">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KH42BRN');</script>
<!-- End Google Tag Manager -->
</head>