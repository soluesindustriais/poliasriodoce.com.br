<?php
$caminhopolias = '<div class=" bg-white breadcrumb container mt-5 pt-5 pb-2" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'produtos-categoria" itemprop="item" title="Produtos">
                <span itemprop="name"> Produtos</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'polias-categoria" itemprop="item" title="Polias">
                <span itemprop="name"> Polias - Categoria </span>
            </a>
            <meta itemprop="position" content="3">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';



$caminhoacoplamentos = '<div class=" bg-white breadcrumb container mt-5 pt-5 pb-2" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'produtos-categoria" itemprop="item" title="Produtos">
                <span itemprop="name"> Produtos</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'acoplamentos-categoria" itemprop="item" title="Acoplamentos ">
                <span itemprop="name"> Acoplamentos - Categoria </span>
            </a>
            <meta itemprop="position" content="3">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';



$caminhobuchas = '<div class=" bg-white breadcrumb container mt-5 pt-5 pb-2" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'produtos-categoria" itemprop="item" title="Produtos">
                <span itemprop="name"> Produtos</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'buchas-categoria" itemprop="item" title="buchas ">
                <span itemprop="name"> Buchas - Categoria </span>
            </a>
            <meta itemprop="position" content="3">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';


?>
?>