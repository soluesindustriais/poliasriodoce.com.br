<header id="topnav" class="defaultscroll sticky">
   <div class="container">
      <!-- Logo container-->
      <div>
         <a class="logo" title="Logo" href="<?= $url ?>"><img class="mw-100" src="imagens/logo/logo.png" alt="Logo"></a>
      </div>
      <div class="buy-button">
         <a id="#btntopo" title="faça parte" href="https://faca-parte.solucoesindustriais.com.br/" target="_blank" class="btn btn-primary">Gostaria de
            anunciar?</a>
      </div>
      <!--end login button-->
      <!-- End Logo container-->
      <div class="menu-extras">
         <div class="menu-item">
            <!-- Mobile menu toggle-->
            <button class="navbar-toggle" aria-label="menu">
               <div class="lines">
                  <span></span>
                  <span></span>
                  <span></span>
               </div>
            </button>
            <!-- End mobile menu toggle-->
         </div>
      </div>
      <div id="navigation">
         <!--end navigation menu-->
         <!-- Navigation Menu-->
         <ul class="navigation-menu">
            <li><a href="<?= $url ?>" title="Início">Início</a></li>

            <li class="has-submenu">
               <a class="d-inline-block" title="Produtos" href="<?= $url ?>produtos-categoria">Produtos</a>
               <span class="menu-arrow"></span>
               <ul class="submenu">

                  <?
                  foreach ($categorias->getCategorias() as $categoria) :
                     $categoriaSemAcento = $trata->trataAcentos($categoria);
                     $categoriaSemHifen = $trata->retiraHifen($categoria);
                  ?>
                     <li class="has-submenu last-elements arrow-element">
                        <a class="d-inline-block" href="<?= $categoriaSemAcento . "-categoria"; ?>"><?= $categoriaSemHifen; ?></a>
                        <span class="submenu-arrow"></span>
                        <ul class="submenu">
                           <? include("inc/" . $categoriaSemAcento . "/" . $categoriaSemAcento . "-sub-menu.php"); ?>
                        </ul>
                     </li>
                  <? endforeach; ?>
               </ul>
            </li>

            <li><a title="sobre nós" href="sobre-nos">Sobre nós</a></li>
            <li><a title="blog" href="blog">Blog</a></li>
            <!--             <li class="has-submenu last-elements">
               <a href="javascript:void(0)">Informações</a><span class="menu-arrow"></span>
               <ul class="submenu megamenu">
                  <li class="active">
                     <ul>
                        <li class="active"><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI </a></li>
                        <li><a href="mpi">MPI</a></li>
                     </ul>
                  </li>
                  <li>
                     <ul>
                        <li class="active"><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI </a></li>
                        <li><a href="mpi">MPI</a></li>
                     </ul>
                  </li>
                  <li>
                     <ul>
                        <li class="active"><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI </a></li>
                        <li><a href="mpi">MPI</a></li>
                     </ul>
                  </li>
               </ul>
            </li> -->
         </ul>
         <!--end navigation menu-->
         <div class="buy-menu-btn d-none">
            <a title="saiba mais" href="" target="_blank" class="btn btn-primary">Saiba mais</a>
         </div>
         <!--end login button-->
      </div>
      <!--end navigation-->
   </div>
   <!--end container-->
</header>
<!--end header-->