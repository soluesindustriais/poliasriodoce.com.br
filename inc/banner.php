        <div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Polia de Ferro</h2>
                <p>Descubra as vantagens da polia de ferro fundido em suas aplicações industriais. Conheça agora a linha completa de polias de ferro em nosso site!</p>
                <a href="<?=$url?>polia-de-ferro" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Procura Acoplamento de Ferro? Nós fazemos</h2>
                <p>O acoplamento de ferro tem como característica facilitar a união entre eixos alinhados ou levemente desalinhados. Sendo assim, é fundamental que a aquisição seja feita em empresas especializadas</p>
                <a href="<?=$url?>acoplamento-de-ferro" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Bucha de Aço!</h2>
                <p>A Bucha de aço foi criada para ser usada nos mais diversos segmentos industriais, por ser feita em aço carbono possui uma durabilidade maior. </p>
                <a href="<?=$url?>bucha-de-aco" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->