<div class="j_imageupload none" id="post_control">
    <div class="j_imageupload_content">
        <form class="j_post_upload" method="post" enctype="multipart/form-data">
            <input type="hidden" name="gallery_rel" value="<?= getModuloId($linkto[0]) ?>"/>                        

            <div class="form-group">
                <label class="control-label col-md-12 col-sm-12 col-xs-12" for="gallery_file">Selecione uma imagem:</label>
                <div class="upload_progress none" style="padding: 5px; background: #00B594; color: #fff; width: 0%; text-align: center; max-width: 100%;">0%</div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input type="file" class="app_post" name="gallery_file" required="required">
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="col-md-9 col-sm-9 col-xs-12">
                <button type="submit" name="Enviar" class="btn btn-primary"><i class="fa fa-upload"></i> Enviar</button>
                <button type="button" class="btn btn-danger j_fechar" id="post_control"><i class="fa fa-close"></i> Fechar</button>
            </div>

        </form>
    </div>
</div>
