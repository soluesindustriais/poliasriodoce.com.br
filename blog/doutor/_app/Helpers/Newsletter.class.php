<?php

/**
 * Newsletter.class.php [MODEL]
 * Classe responsável por gerir as newsletter cadastradas para os projetos.
 * @copyright (c) 2017, Rafael da Silva Lima & Doutores da Web
 */
class Newsletter {

  //Tratamento de resultados e mensagens
  private $Result;
  private $Error;
  private $Mensagem;
  //Entrada de dados
  private $Data;
  private $Id;

  /**
   * <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
   * Função compativel com AJAX
   * @param array $Data
   */
  public function Cadastrar(array $Data) {
    $this->Data = $Data;
    $this->CheckData();
  }

  /**
   * <b>Função que ativa o cadastro da newsletter</b>
   * Responsável por validar a chave enviado por e-mail para quem se cadastro na newsletter via frontend
   * @param string $Data
   */
  public function Ativa($Data) {
    $this->Data = strip_tags(trim($Data));

    $Read = new Read;
    $Read->ExeRead(TB_NEWSLETTER, "WHERE news_key = :key AND news_status = :st AND user_empresa = :emp", "key={$this->Data}&st=1&emp=" . EMPRESA_CLIENTE);
    if ($Read->getResult()):
      $this->Data = $Read->getResult();
      $this->AlteraStatus();
    else:
      $this->Error = array("Não identificamos a chave de e-mail cadastrado ou ela já foi confirmada.", WS_ERROR, "Aviso!");
      $this->Result = false;
    endif;
  }

  public function Remover($Id) {
    $this->Id = (int) $Id;
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }

  ########################################
  ########### METODOS PRIVADOS ###########
  ########################################
  //Verifica a integridade dos dados e direciona as operações

  private function CheckData() {
    if (in_array('', $this->Data)):
      $this->Error = array("Todos os campos são obrigatórios!", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif (!Check::Email($this->Data['news_email'])):
      $this->Error = array("Digite um endereço de e-mail válido.", WS_ALERT, "Alerta!");
      $this->Result = false;
    else:
      $this->CheckEmailRepeat();
    endif;
  }

  //Verifica se o e-mail já existe e se existir, reenvia a chave de ativação caso esteja desativado  
  private function CheckEmailRepeat() {
    $Read = new Read;
    $Read->ExeRead(TB_NEWSLETTER, "WHERE news_email = :email AND user_empresa = :emp", "email={$this->Data['news_email']}&emp={$this->Data['user_empresa']}");
    if (!$Read->getResult()):
      $this->GetKey();
      $this->Create();
    else:
      $this->GetKey();
      $this->SendAtivacao();
    endif;
  }

  //Cadastra os dados do relatorio no banco
  private function Create() {
    $Create = new Create;
    $Create->ExeCreate(TB_NEWSLETTER, $this->Data);
    if (!$Create->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->SendAtivacao();
    endif;
  }

  private function SendAtivacao() {

    //Paramentos a serem enviados dentro do template view formato Array
    $this->Mensagem['MENSAGEM'] = CADASTRO_NEWSLETTER;
    $this->Mensagem['SITENAME'] = SITE_NAME;
    $this->Mensagem['RAIZ'] = RAIZ;
    $this->Mensagem['URL'] = 'index.php?newsActive=' . $this->Data['news_key'];

    $view = new View;
    $tpl = $view->Load('email/cadastronewsletter');

    $SendMail = new Email;
    
    //Define o cabeçalho e a mensagem do e-mail que pode ser vinda de um tpl
    $this->Mensagem['Assunto'] = 'E-mail cadastrado - ' . SITENAME;
    $this->Mensagem['DestinoNome'] = $this->Data['news_nome'];
    $this->Mensagem['DestinoEmail'] = $this->Data['news_email'];
    $this->Mensagem['RemetenteNome'] = REMETENTENOME;
    $this->Mensagem['RemetenteEmail'] = REMETENTEMAIL;
    $this->Mensagem['Mensagem'] = $view->Show($this->Mensagem, $tpl);

    $SendMail->Enviar($this->Mensagem);

    if (!$SendMail->getResult()):
      $this->Error = array("Desculpe, houve um erro ao enviar o e-mail de confirmação, po favor tente novamente mais tarde.", WS_INFOR, "Aviso!");
      $this->Result = false;
    else:
      $this->Error = array("Olá {$this->Data['news_nome']}, obrigado por se cadastrar, enviamos um e-mail de confirmação para o endereço {$this->Data['news_email']}.", WS_ALERT, "Tudo certo!");
      $this->Result = true;
    endif;
  }

  private function GetKey() {
    $Read = new Read;
    $Read->ExeRead(TB_NEWSLETTER, "WHERE news_email = :email AND user_empresa = :emp AND news_status = 1", "email={$this->Data['news_email']}&emp={$this->Data['user_empresa']}");
    if (!$Read->getResult()):
      $this->Data['news_key'] = substr(md5(time() + $this->Data['news_email']), 0, 25);
    else:
      $read = $Read->getResult();
      $this->Data['news_key'] = substr(md5(time() + $this->Data['news_email']), 0, 25);
      $this->Id = $read[0]['news_id'];
      $this->Update();
    endif;
  }

  private function Update() {
    $Update = new Update;
    $Update->ExeUpdate(TB_NEWSLETTER, $this->Data, "WHERE news_id = :id", "id={$this->Id}");
    if (!$Update->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    endif;
  }

  private function AlteraStatus() {    
    $up = array('news_status' => 2, 'news_key' => null);
    $Update = new Update;
    $Update->ExeUpdate(TB_NEWSLETTER, $up, "WHERE news_key = :key AND user_empresa = :emp", "key={$this->Data[0]['news_key']}&emp=" . EMPRESA_CLIENTE);
    if (!$Update->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Error = array("Obrigado! Seu e-mail foi confirmado com sucesso, aguarde por nosso boletins.", WS_ACCEPT, "Tudo certo!");
      $this->Result = true;
    endif;
  }

}
