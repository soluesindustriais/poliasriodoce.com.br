<?php

/**
 * GravaImport.class.php [CLASSE]
 * <b>Gravar importação</b>
 * Classe responsável por gravar os itens da importação no banco, tratando os campos
 * @copyright (c) 2017, Rafael da Silva Lima & Doutores da Web
 */
class GravaImport {

  private $Data;
  private $File;
  private $Result;
  private $Error;
  private $Callback;
  private $Configs;
  private $Prefixo;
  private $Linha;

  /**
   * <b>Válida e salva os dados no banco</b>
   * @param jSon $Dados = Entrada de dados em Json.
   */
  public function saveData($Dados, $Configs, $Linha) {
    $this->Data = (array) json_decode($Dados);
    $this->Configs = (array) json_decode($Configs);
    $this->Linha = (int) $Linha;
    $this->CheckData();
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }
  
  /**
   * <b>Retorna o relatório</b>
   * São retornados informações de toda a operação com Falhas, Alertas e Sucessos.
   * Podem haver mais de uma falha, Alertas por registro.
   * @return array = Relatório da operação envelopado em um array
   */
  public function getCallback() {
    return $this->Callback;
  }

  
  ########################################
  ########### METODOS PRIVADOS ###########
  ########################################

  private function CheckData() {
    $this->SetFiles();
    $this->SetAtributos();
    $this->SendFile();
  }

  private function SetFiles() {

    foreach ($this->Data as $keys => $value):
      if (strpos($keys, "cover") !== false || strpos($keys, "file") !== false):
        $this->Prefixo = $keys;
        $this->File['name'] = (!empty($value) && isset($value) ? $value : 'default.png');
      endif;
    endforeach;

    $this->File['tmp_name'] = Check::SetDiretorio(__DIR__, 2) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . '0' . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . Check::SetDiretorio($this->File['name']);

    if (file_exists($this->File['tmp_name']) && !is_dir($this->File['tmp_name'])):
      $this->File['type'] = mime_content_type($this->File['tmp_name']);
      $this->File['size'] = filesize($this->File['tmp_name']);
      $this->File['name'] = $this->File['name'];
    else:
      $this->File['tmp_name'] = Check::SetDiretorio(__DIR__, 2) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . '0' . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . Check::GetFileRemoto($this->File['name']);
      $this->File['type'] = mime_content_type($this->File['tmp_name']);
      $this->File['size'] = filesize($this->File['tmp_name']);
      $this->File['name'] = 'default.png';
    endif;

    $this->Data[$this->Prefixo] = $this->File;
  }

  private function SetAtributos() {
    $this->Data['cat_parent'] = $this->Configs['cat_parent'];
    $this->Data['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
  }

  private function SendFile() {
    $typesImage = array(
      'image/jpg',
      'image/jpeg',
      'image/pjpeg',
      'image/png',
      'image/x-png'
    );

    //Verifica se é imagem
    if (in_array($this->File['type'], $typesImage)):
      $this->EnviaImagem();
    else:
      $this->EnviaArquivo();
    endif;
    
    $this->Create();    
  }

  private function EnviaImagem() {
    if (!empty($this->File['tmp_name'])):
      $Upload = new Upload($this->Configs['baseDir']);
      $ImgName = "cover-{$this->File['name']}-" . (substr(md5(time() + $this->File['name']), 0, 10));
      $Upload->Image($this->Data[$this->Prefixo], $ImgName, 800, $_SESSION['userlogin']['user_empresa'], 'importados');
      if ($Upload->getError()):
        //Registra no callback a linha que não pode ser gravada
        $this->Callback['Falha'][$this->Linha][] = 'Não pode ser gravada: ' . json_encode($Upload->getError());
      else:
        $this->Data[$this->Prefixo] = $Upload->getResult();
      endif;
    else:
      $this->Callback['Alerta'][$this->Linha] = 'Não foi possível enviar a imagem deste registro, verifique se a imagem temporarária existe em uploads/0/import/ ';
    endif;
  }

  private function EnviaArquivo() {
    if (!empty($this->File['tmp_name'])):
      $Upload = new Upload($this->Configs['baseDir']);
      $FileName = "file-{$this->File['name']}-" . (substr(md5(time() + $this->File['name']), 0, 10));
      $Upload->File($this->Data[$this->Prefixo], $FileName, $_SESSION['userlogin']['user_empresa'], 'importados', 15);
      if ($Upload->getError()):
        //Registra no callback a linha que não pode ser gravada
        $this->Callback['Falha'][$this->Linha][] = 'Não pode ser gravada: ' . json_encode($Upload->getError());
      else:
        $this->Data[$this->Prefixo] = $Upload->getResult();
      endif;
    else:
      $this->Callback['Alerta'][$this->Linha] = 'Não foi possível enviar a imagem deste registro, verifique se a imagem temporarária existe em uploads/0/import/ ';
    endif;
  }

  //Cadastra os dados no banco
  private function Create() {
    $Create = new Create;
    $Create->ExeCreate(constant($this->Configs['table']), $this->Data);

    if (!$Create->getResult()):
      //Registra no callback a linha que não pode ser gravada
      $this->Callback['Falha'][$this->Linha][] = 'Não pode ser gravada: ' . json_encode($this->Data);
      $this->Result = false;
    else:
      $this->Callback['Sucesso'][$this->Linha] = $Create->getResult() . ' - ' . json_encode($this->Data);
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Importador", "Importou para a tabela {$this->Configs['table']}", date("Y-m-d H:i:s"));
      $this->Result = true;
    endif;
  }

}
