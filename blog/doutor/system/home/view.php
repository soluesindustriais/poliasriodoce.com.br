<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-file-text-o"></i> Lista de páginas</h3>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12"> 
                <div class="x_panel">

                    <div class="x_content">
                        <div class="x_title">
                            <h2>Suas páginas cadastras.<small>Você também pode acessar, editar, remover e alterar status das páginas.</small></h2>                           
                            <div class="clearfix"></div>                            
                        </div>
                        <br/>                       
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                        unset($_SESSION['Error']);
                    endif;

                    $ReadRecursos = new Read;
                    $ReadRecursos->ExeRead(TB_PAGINA, "WHERE user_empresa = :emp ORDER BY pag_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}");
                    if (!$ReadRecursos->getResult()):
                        WSErro("Desculpe mas não foi encontrado nenhuma categoria no sistema.", WS_INFOR, null, SITENAME);
                    else:
                        ?> 
                        <div class="clearfix"></div>
                        <!-- start accordion -->
                        <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php
                            foreach ($ReadRecursos->getResult() as $key):
                                extract($key);
                                ?>
                                <div class="panel j_item" id="<?= $pag_id; ?>">
                                    <a class="panel-heading" role="tab" id="CatHeading<?= $pag_id; ?>" data-toggle="collapse" data-parent="#accordion" href="#Cat-<?= $pag_id; ?>" aria-expanded="true" aria-controls="Cat-<?= $pag_id; ?>">
                                        <h4 class="panel-title">Página: <strong><?= $pag_title; ?></strong>                                                 <div class="pull-right col-md-1">                                                
                                                <p class="j_GetStatusRecursos" rel="<?= $pag_id; ?>"><?= $pag_status; ?></p>
                                        </h4>
                                    </a>
                                    <div id="Cat-<?= $pag_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="CatHeading<?= $pag_id; ?>">
                                        <div class="panel-body">
                                            <div class="pull-left col-md-12 col-sm-12 col-xs-12">                                                
                                                <p><?= $pag_description; ?></p>
                                            </div>       

                                            <div class="clearfix"></div>

                                            <div class="pull-right"> 
                                                <a class="btn btn-dark" href="../<?= $pag_name; ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                                <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=paginas/update&id=<?= $pag_id; ?>'"><i class="fa fa-pencil"></i></button>
                                                <button type="button" class="btn btn-danger j_remove" rel="<?= $pag_id; ?>" action="RemovePagina"><i class="fa fa-trash"></i></button>
                                                <button type="button" class="btn j_statusRecursos" rel="<?= $pag_id; ?>" tabindex="getStatusPag" action="StatusPag" value="<?= $pag_status; ?>"><i class="fa fa-ban"></i></button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>  
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!-- end of accordion -->                     
                    <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
