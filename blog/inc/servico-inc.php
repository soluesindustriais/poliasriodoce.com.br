<div class="service-inc htmlchars">
  <?php
  $Read->ExeRead(TB_SERVICO, "WHERE user_empresa = :emp AND serv_status = :stats AND serv_name = :nm", "emp=" . EMPRESA_CLIENTE . "&stats=2&nm={$lastCategory}");
  if (!$Read->getResult()):
    WSErro("Desculpe, mas não foi encontrando o conteúdo relacionado a esta página, volte mais tarde", WS_INFOR, null, "Aviso!");
  else:
    foreach ($Read->getResult() as $dados):
      extract($dados); ?>
      <div class="container">
        <div class="row">
          <div class="col-6 col-md-12 p-5">
            <div class="service-inc__cover">
              <a href="<?= RAIZ . '/doutor/uploads/' . $serv_cover; ?>" class="lightbox" title="<?= $serv_title; ?>">
                <?= Check::Image('doutor/uploads/' . $serv_cover, $serv_title, 'picture-center border p-1', 600, 500); ?>
              </a>
            </div>
            <div class="row justify-content-end justify-content-md-center">
              <a class="btn btn-orc fs-16" href="<?=$url?>contato" title="Faça se orçamento agora!"><strong>Faça seu orçamento agora mesmo!</strong> <i class="fas fa-arrow-right ml-2" aria-hidden="true"></i></a>
            </div>
          </div>
          <div class="col-6 col-md-12 p-5">
            <?php
              $serv_content = preg_replace('/(src=")(.*)(\/doutor\/uploads\/2\/)/mU', '$1'.RAIZ.'$3', $serv_content);
              $serv_content = str_replace('src="doutor', 'src="'.RAIZ.'/doutor', $serv_content);
              echo $serv_content;
            ?>
            <?php if ($url_relation != 0):
              $prodDownloads = array();
              $Read = new Read;
              $url_relation = explode(',', trim($url_relation, ','));
              foreach ($url_relation as $urlsr):
                $Read->ExeRead(TB_DOWNLOAD, "WHERE dow_id = :id AND user_empresa = :emp ORDER BY dow_title ASC", "id={$urlsr}&emp=" . EMPRESA_CLIENTE);
                if ($Read->getResult()):
                  foreach ($Read->getResult() as $downs):
                    array_push($prodDownloads, array("dow_title" => $downs['dow_title'], "dow_file" => $downs['dow_file']));
                  endforeach;
                endif;
              endforeach;
            endif; ?>
            <?php if(count($prodDownloads) > 0):?>
              <h3><strong>Confira os downloads disponíveis:</strong></h3>
              <?php foreach ($prodDownloads as $downloadItem): ?>
                <div class="row">
                  <a href="<?= RAIZ . "/doutor/uploads/" . $downloadItem['dow_file']; ?>" title="<?= $downloadItem['dow_title']; ?>" class="btn mt-0" target="_blank"><i class="fas fa-download"></i> <?= $downloadItem['dow_title']; ?></a>
                </div>
              <?php endforeach; ?>
            <?php endif; ?>
          </div>
          <div class="col-12 p-5">
            <?php
            $Read->ExeRead(TB_GALLERY, "WHERE gallery_rel = :id AND user_empresa = :emp AND cat_parent IN(:cat)", "id={$serv_id}&emp=" . EMPRESA_CLIENTE . "&cat={$cat_parent}");
            if ($Read->getResult()): ?>
              <h2>Confira mais imagens:</h2>
              <div class="service-inc__gallery">
                <?php foreach ($Read->getResult() as $gallery): ?>
                  <?php extract($gallery); ?>
                  <div class="gallery__item">
                    <a href="<?= RAIZ . '/doutor/uploads/' . $gallery_file; ?>" data-fancybox="group1" class="lightbox" title="<?= $serv_title; ?>">
                      <?= Check::Image('doutor/uploads/' . $gallery_file, $serv_title, 'picture-center w-100 h-100 object-fit-cover border p-1', 300, 200); ?>
                    </a>
                  </div>
                <?php endforeach; ?>
              </div>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div> <!-- htmlchars -->
<?php include('inc/aside-sig-servicos.php'); ?>
<script>
  <? include ('slick/slick.min.js'); ?>
  $(document).ready(function() {
    $('.service-inc__gallery').slick({
      autoplaySpeed: 3000,
      autoplay: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      cssEase: 'ease',
      arrows: true,
      responsive: [{breakpoint: 577, settings: {slidesToShow: 1, centerMode: true}}]
    });
  });
</script>
